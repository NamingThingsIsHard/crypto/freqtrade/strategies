"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from mixins.dmi import DMIBuyMixin
from mixins.moving_average import MovingAverageBuySwingMixin, MovingAverageSellSwingMixin


class Swing(
    MovingAverageBuySwingMixin,
    MovingAverageSellSwingMixin,
):
    """
    Uses two moving averages and enters/exits when they cross
    """
    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    trailing_stop = True
    trailing_stop_positive = 0.02
    trailing_stop_positive_offset = 0.03
    trailing_only_offset_is_reached = True
    timeframe = "1h"

    minimal_roi = {
        "0": 10.0
    }


class SwingDMI(
    DMIBuyMixin,
    Swing,
):
    """
    Uses moving average trend swings to buy.
    When then trend swings from falling to rising, it tries to buy.

    Uses DMI to trade only in positive trends.
    """

    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    trailing_stop = True
    trailing_stop_positive = 0.01
    trailing_stop_positive_offset = 0.03
    trailing_only_offset_is_reached = True
    timeframe = "1h"

    minimal_roi = {
        "0": 10.0
    }

    @property
    def protections(self):
        return [
            {
                "method": "LowProfitPairs",
                "lookback_period_candles": 24 * 7,
                "trade_limit": 2,
                "stop_duration_candles": 48,
                "required_profit": 0.02
            }
        ]