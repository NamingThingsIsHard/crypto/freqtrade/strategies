"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from functools import reduce

import pandas as pd
import talib.abstract as ta
from freqtrade.strategy import CategoricalParameter, IStrategy, IntParameter
from pandas import DataFrame
from technical import qtpylib


class MA_RSI(IStrategy):
    """
    Uses two moving averages and enters/exits when they cross

    Uses RSI to try and stabilize in volatile markets
    """
    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    trailing_stop = True
    trailing_stop_positive = 0.02
    trailing_stop_positive_offset = 0.03
    trailing_only_offset_is_reached = True
    timeframe = "1h"

    buy_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="TEMA")
    buy_ma_period = IntParameter(10, 60, default=30)

    sell_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="TEMA")
    sell_ma_period = IntParameter(10, 60, default=30)

    buy_rsi_enable = CategoricalParameter([True, False], default=True)
    buy_rsi = IntParameter(20, 40, default=30)
    buy_rsi_period = IntParameter(10, 20, default=15)

    sell_rsi_enable = CategoricalParameter([True, False], default=True)
    sell_rsi = IntParameter(60, 100, default=70)
    sell_rsi_period = IntParameter(10, 20, default=15)

    minimal_roi = {
        "0": 10.0
    }

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        frames = [dataframe]

        # Sell indicators
        for func_name in self.buy_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.buy_ma_period.range:
                frames.append(DataFrame({
                    f"buy_{func_name}_{val}": function(dataframe, timeperiod=val)
                }))

        for buy_rsi in self.buy_rsi_period.range:
            frames.append(DataFrame({
                f"buy_rsi_{buy_rsi}": ta.RSI(dataframe, timeperiod=buy_rsi)
            }))

        for func_name in self.sell_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.sell_ma_period.range:
                frames.append(DataFrame({
                    f"sell_{func_name}_{val}": function(dataframe, timeperiod=val)
                }))

        for sell_rsi in self.sell_rsi_period.range:
            frames.append(DataFrame({
                f"sell_rsi_{sell_rsi}": ta.RSI(dataframe, timeperiod=sell_rsi)
            }))

        toreturn = pd.concat(frames, axis=1)
        return toreturn

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        conditions = []
        # Guard
        if self.buy_rsi_enable.value:
            conditions.append(dataframe[f"buy_rsi_{self.buy_rsi_period.value}"] > self.buy_rsi.value)

        # Trigger
        #  bp  p  c
        #  x      x
        #      x
        moving_average = dataframe[f"buy_{self.buy_ma_type.value}_{self.buy_ma_period.value}"]
        conditions.append(
            qtpylib.crossed_above(moving_average, moving_average.shift(1))
        )
        dataframe.loc[
            reduce(lambda x, y: x & y, conditions),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        conditions = []

        # "Guard"
        if self.sell_rsi_enable.value:
            conditions.append(dataframe[f'sell_rsi_{self.sell_rsi_period.value}'] < self.sell_rsi.value)

        # Trigger
        moving_average = dataframe[f"sell_{self.sell_ma_type.value}_{self.sell_ma_period.value}"]
        conditions.append(
            qtpylib.crossed_below(moving_average, moving_average.shift(1))
        )

        dataframe.loc[
            reduce(lambda x, y: x & y, conditions),
            'sell'] = 1

        return dataframe
