"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from typing import List

import talib.abstract as ta
from pandas import DataFrame

from freqtrade.strategy import CategoricalParameter, IntParameter
from mixins.base import MixinStrategy
from mixins.dmi import DMIBuyMixin
from mixins.min_roi_stoploss import MinRoiStoplossMixin


class MA_DMI(
    DMIBuyMixin,
    MinRoiStoplossMixin,
    MixinStrategy,
):
    """
    Uses moving average trend swings to buy.
    When then trend swings from falling to rising, it tries to buy.

    Uses DMI to trade only in positive trends.
    """

    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    trailing_stop = True
    trailing_stop_positive = 0.02
    trailing_stop_positive_offset = 0.03
    trailing_only_offset_is_reached = True
    timeframe = "1h"

    buy_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="TEMA")
    buy_ma_period = IntParameter(10, 60, default=30)
    """The threshold that has to be passed"""

    minimal_roi = {
        "0": 10.0
    }

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)
        # Buy indicators
        for func_name in self.buy_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.buy_ma_period.range:
                frames.append(DataFrame({
                    f"buy_{func_name}_{val}": function(dataframe, timeperiod=val)
                }))
        return frames

    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_buy_conditions(dataframe, metadata)

        # Buy in MA uptrend
        moving_average = dataframe[f"buy_{self.buy_ma_type.value}_{self.buy_ma_period.value}"]
        conditions.append(
            moving_average > moving_average.shift(1)
        )
        return conditions

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict):
        return super(MA_DMI, self).populate_sell_conditions(dataframe, metadata)
