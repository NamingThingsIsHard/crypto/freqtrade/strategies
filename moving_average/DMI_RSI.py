"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from pandas import DataFrame

from .DMI import MA_DMI
from mixins.rsi import RSIMixin


class MA_DMI_RSI(RSIMixin, MA_DMI):
    """
    Uses moving average trend swings to buy.
    When then trend swings from falling to rising, it tries to buy.

    Uses DMI to trade only in positive trends
    Uses RSI to limit to good positive trends
    """

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict):
        return []
