"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from mixins.dmi import DMIBuyMixin, DMISellMixin
from mixins.macd import MACDMixin
from mixins.rsi import RSIMixin


class MACD(MACDMixin):
    """
    Uses two moving averages and enters/exits when they cross
    """
    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    timeframe = "1h"

    minimal_roi = {
        "0": 10
    }


class MACD_DMI(DMIBuyMixin, DMISellMixin, MACDMixin):
    stoploss = -0.05
    timeframe = "1h"

    minimal_roi = {
        "0": 10
    }

    protections = [
        {
            "method": "LowProfitPairs",
            "lookback_period_candles": 24 * 7,
            "trade_limit": 2,
            "stop_duration_candles": 48,
            "required_profit": 0.02
        }
    ]


class MACD_RSI(RSIMixin, MACDMixin):
    stoploss = -0.05
    timeframe = "1h"

    minimal_roi = {
        "0": 10
    }

    protections = [
        {
            "method": "LowProfitPairs",
            "lookback_period_candles": 24 * 7,
            "trade_limit": 2,
            "stop_duration_candles": 48,
            "required_profit": 0.02
        }
    ]
