"""
freq_course
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from typing import Optional


def get_minimal_roi_stoploss(
        min_roi_pct: float, max_trailing_stoploss: float,
        open_rate: float, current_rate: float,
) -> Optional[float]:
    """
    Calculate a stoploss once the ROI hits a minimum

    :param min_roi_pct: the offset and minimum ROI
    :param max_trailing_stoploss: How large the stoploss can get at most
    :param open_rate: How much it cost when opening the trade
    :param current_rate: How much it currently costs
    :return: A stoploss if applicable
    """
    current_profit = current_rate / open_rate
    min_roi_total_pct = 1 + min_roi_pct
    # Make sure we reach the minimal ROI first
    if current_profit < min_roi_total_pct:
        return

    min_roi_rate = open_rate * (1 + min_roi_pct)
    stoploss = 1 - (min_roi_rate / current_rate)
    return min(max_trailing_stoploss, stoploss)


class TestMinimalROIStoploss(unittest.TestCase):

    def test_not_reached(self):
        """
        The current price hasn't reached the minimal ROI rate so we should return nothing

           open     current   minROI
            │         │           │
            └─────────┴───────────┘
        """
        self.assertIsNone(get_minimal_roi_stoploss(
            min_roi_pct=0.02,
            max_trailing_stoploss=0.03,
            open_rate=100,
            current_rate=101,
        ))

    def test_reached_below_max(self):
        """
        The current price has passed the minimal ROI rate and stop loss should be minROI
         open               minROI   current
          │                     │       │
          └─────────────────────┼───────┘
                                │
                            stoploss
        """
        max_trailing_stoploss = 0.03
        open_rate = 100
        current_rate = 103
        min_roi_pct = 0.02
        stoploss = get_minimal_roi_stoploss(min_roi_pct=min_roi_pct,
                                            max_trailing_stoploss=max_trailing_stoploss,
                                            open_rate=open_rate,
                                            current_rate=current_rate)
        minimal_roi_rate = open_rate * (1 + min_roi_pct)
        stoploss_value = current_rate * (1 - stoploss)
        self.assertAlmostEqual(minimal_roi_rate, stoploss_value, 1)
        self.assertLess(stoploss, max_trailing_stoploss)

    def test_reached_max(self):
        """
        The current price passed the minimal ROI and the stoploss shouldn't be set at minimal ROI

         open      minROI                  current
          │            │                      │
          └────────────┴──────────┬───────────┘
                                  │
                              stoploss
        """
        max_trailing_stoploss = 0.03
        min_roi_pct = 0.02
        open_rate = 100
        current_rate = 110
        stoploss = get_minimal_roi_stoploss(min_roi_pct=min_roi_pct,
                                            max_trailing_stoploss=max_trailing_stoploss,
                                            open_rate=open_rate,
                                            current_rate=current_rate)
        self.assertEqual(stoploss, max_trailing_stoploss)

        minimal_roi_rate = open_rate * min_roi_pct
        stoploss_value = current_rate * (1 - stoploss)
        self.assertGreater(stoploss_value, minimal_roi_rate)
