from datetime import datetime

from freqtrade.persistence import Trade
from freqtrade.strategy import IStrategy
from stoploss.min_roi import get_minimal_roi_stoploss


class MinRoiStoplossMixin(IStrategy):
    use_custom_stoploss = True

    def custom_stoploss(self, pair: str, trade: Trade, current_time: datetime, current_rate: float,
                        current_profit: float, **kwargs) -> float:
        stoploss = None
        # TODO: maybe use another config attribute
        if "sell_profit_only" in self.config and (
                minimal_roi := self.config.get("sell_profit_offset")
        ):
            stoploss = get_minimal_roi_stoploss(
                min_roi_pct=minimal_roi,
                max_trailing_stoploss=self.trailing_stop_positive,
                open_rate=trade.open_rate,
                current_rate=current_rate
            )
        return stoploss if stoploss else self.stoploss
