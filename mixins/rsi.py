from typing import List

import talib.abstract as ta
from pandas import DataFrame

from freqtrade.strategy import IntParameter, CategoricalParameter
from .base import MixinStrategy


class RSIMixin(MixinStrategy):
    buy_rsi_enable = CategoricalParameter([True, False], default=True)
    buy_rsi = IntParameter(20, 40, default=30)
    buy_rsi_period = IntParameter(10, 20, default=15)

    sell_rsi_enable = CategoricalParameter([True, False], default=True)
    sell_rsi = IntParameter(60, 100, default=75)
    sell_rsi_period = IntParameter(10, 20, default=15)

    def populate_indicator_frames(self, dataframe: DataFrame, ) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)

        for buy_rsi in self.buy_rsi_period.range:
            frames.append(DataFrame({
                f"buy_rsi_{buy_rsi}": ta.RSI(dataframe, timeperiod=buy_rsi)
            }))

        for sell_rsi in self.sell_rsi_period.range:
            frames.append(DataFrame({
                f"sell_rsi_{sell_rsi}": ta.RSI(dataframe, timeperiod=sell_rsi)
            }))

        return frames

    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_buy_conditions(dataframe, metadata)
        # Guard
        if self.buy_rsi_enable.value:
            conditions.append(dataframe[f"buy_rsi_{self.buy_rsi_period.value}"] > self.buy_rsi.value)

        return conditions

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_sell_conditions(dataframe, metadata)
        # Guard
        if self.sell_rsi_enable.value:
            conditions.append(dataframe[f"sell_rsi_{self.sell_rsi_period.value}"] < self.sell_rsi.value)

        return conditions
