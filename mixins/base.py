from abc import abstractmethod
from functools import reduce
from typing import List

import pandas as pd
from pandas import DataFrame, Series

from freqtrade.strategy import IStrategy


class MixinStrategy(IStrategy):
    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Default implementation of abstract parent

        Populate the indicators in populate_indicator_frame
        """
        return pd.concat(self.populate_indicator_frames(dataframe), axis=1)

    @abstractmethod
    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        """
        Create indicators and put them in a list

        We do this in order to merge them all later and save processing time.

        Subclasses should probably extend the list returned from the parent/super implementation.
        If you choose not to, you must AT LEAST include the dataframe in the list.

        :param dataframe: The OHCLV values
        :return: Calculated indicators
        """
        frames = [dataframe]

        # Example
        # for func_name in self.buy_ma_type.range:
        #     function = getattr(ta, func_name)
        #     for val in self.buy_ma_period.range:
        #         frames.append(DataFrame({
        #             f"buy_{func_name}_{val}": function(dataframe, timeperiod=val)
        #         }))

        return frames

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Default implementation of the parent that uses populate_buy_conditions to allow subclasses to use mixins

        See the parent documentation to know more about the params
        """

        conditions = self.populate_buy_conditions(dataframe, metadata)
        dataframe.loc[reduce(lambda x, y: x & y, conditions), 'buy'] = 1

        return dataframe

    @abstractmethod
    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        """
        A method to be overriden by subclasses in order to list the conditions necessary for a buy

        Subclasses should always call the super method first in order for python to call other mixins,
         should those exist
        """
        return []

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        conditions = self.populate_sell_conditions(dataframe, metadata)
        if conditions:
            dataframe.loc[reduce(lambda x, y: x & y, conditions), 'sell'] = 1

        return dataframe

    @abstractmethod
    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        """
        A method to be overridden by subclasses in order to list the conditions necessary for a sell

        Subclasses should always call the super method first in order for python to call other mixins,
         should those exist
        """
        return []
