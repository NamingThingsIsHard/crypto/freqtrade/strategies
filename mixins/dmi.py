from typing import List

import talib.abstract as ta
from pandas import DataFrame
from technical import qtpylib

from freqtrade.strategy import IntParameter
from .base import MixinStrategy


class DMIBuyParams:
    buy_plus_di = IntParameter(10, 30, default=14)
    buy_minus_di = IntParameter(10, 30, default=14)
    buy_adx_period = IntParameter(10, 30, default=14)
    buy_adx_threshold = IntParameter(20, 35, default=25)
    """The threshold that has to be passed"""

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)
        # DMI params
        param_mapping = [
            (self.buy_plus_di, "PLUS_DI"),
            (self.buy_minus_di, "MINUS_DI"),
            (self.buy_adx_period, "ADX"),
        ]
        for param, func_name in param_mapping:
            function = getattr(ta, func_name)
            for val in param.range:
                frames.append(DataFrame({
                    f"buy_{func_name.lower()}_{val}": function(dataframe, timeperiod=val)
                }))
        return frames


class DMISellParams:
    sell_plus_di = IntParameter(10, 30, default=14)
    sell_minus_di = IntParameter(10, 30, default=14)
    sell_adx_period = IntParameter(10, 30, default=14)
    sell_adx_threshold = IntParameter(20, 35, default=25)
    """The threshold that has to be passed"""

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)
        # DMI params
        param_mapping = [
            (self.sell_plus_di, "PLUS_DI"),
            (self.sell_minus_di, "MINUS_DI"),
            (self.sell_adx_period, "ADX"),
        ]
        for param, func_name in param_mapping:
            function = getattr(ta, func_name)
            for val in param.range:
                frames.append(DataFrame({
                    f"sell_{func_name.lower()}_{val}": function(dataframe, timeperiod=val)
                }))
        return frames


class DMIBuyMixin(DMIBuyParams, MixinStrategy):

    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_buy_conditions(dataframe, metadata)
        conditions.append(
            dataframe[f"buy_adx_{self.buy_adx_period.value}"] > self.buy_adx_threshold.value
        )

        conditions.append(
            dataframe[f"buy_plus_di_{self.buy_plus_di.value}"] > dataframe[f"buy_minus_di_{self.buy_minus_di.value}"]
        )
        return conditions


class DMISellMixin(DMISellParams, MixinStrategy):

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_buy_conditions(dataframe, metadata)
        conditions.append(
            dataframe[f"sell_adx_{self.sell_adx_period.value}"] > self.sell_adx_threshold.value
        )

        conditions.append(
            dataframe[f"sell_plus_di_{self.sell_plus_di.value}"] < dataframe[f"sell_minus_di_{self.sell_minus_di.value}"]
        )
        return conditions


class DMICrossoverMixin(DMIBuyParams, DMISellParams, MixinStrategy):
    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_buy_conditions(dataframe, metadata)

        # Guard
        conditions.append(
            dataframe[f"buy_buy_adx_{self.buy_adx_period.value}"] > self.buy_adx_threshold.value
        )
        conditions.append(
            qtpylib.crossed_above(
                dataframe[f"buy_plus_di_{self.buy_plus_di.value}"],
                dataframe[f"buy_minus_di_{self.buy_minus_di.value}"]
            )
        )
        return conditions

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict):
        conditions = super().populate_sell_conditions(dataframe, metadata)

        # Guard
        conditions.append(
            dataframe[f"sell_adx_{self.sell_adx_period.value}"] > self.sell_adx_threshold.value
        )
        conditions.append(
            qtpylib.crossed_below(
                dataframe[f"sell_plus_di_{self.sell_plus_di.value}"],
                dataframe[f"sell_minus_di_{self.sell_minus_di.value}"]
            )
        )
        return conditions
