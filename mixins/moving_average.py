from typing import List

import talib.abstract as ta
from pandas import DataFrame, Series
from technical import qtpylib

from freqtrade.strategy import CategoricalParameter, IntParameter
from mixins.base import MixinStrategy


class MovingAverageBuyParams:
    buy_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="TEMA")
    buy_ma_period = IntParameter(10, 60, default=30)


class MovingAverageBuyMixin(MovingAverageBuyParams, MixinStrategy):

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)
        for func_name in self.buy_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.buy_ma_period.range:
                frames.append(DataFrame({
                    f"buy_{func_name}_{val}": function(dataframe, timeperiod=val)
                }))
        return frames


class MovingAverageBuySwingMixin(MovingAverageBuyMixin, MixinStrategy):
    """Buy when the moving average swings from a downtrend to a uptrend"""

    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        conditions = super().populate_buy_conditions(dataframe, metadata)

        moving_average = dataframe[f"buy_{self.buy_ma_type.value}_{self.buy_ma_period.value}"]
        conditions.append(qtpylib.crossed_above(moving_average, moving_average.shift(1)))

        return conditions


class MovingAverageSellParams:
    sell_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="TEMA")
    sell_ma_period = IntParameter(10, 60, default=30)


class MovingAverageSellMixin(MovingAverageSellParams, MixinStrategy):

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        frames = super().populate_indicator_frames(dataframe)
        for func_name in self.sell_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.sell_ma_period.range:
                frames.append(DataFrame({
                    f"sell_{func_name}_{val}": function(dataframe, timeperiod=val)
                }))
        return frames


class MovingAverageSellSwingMixin(MovingAverageSellMixin, MixinStrategy):
    """Sell when the moving average swings from an uptrend to a downtrend"""

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        conditions = super().populate_buy_conditions(dataframe, metadata)

        moving_average = dataframe[f"sell_{self.sell_ma_type.value}_{self.sell_ma_period.value}"]
        conditions.append(qtpylib.crossed_below(moving_average, moving_average.shift(1)))

        return conditions
