import itertools
from typing import List, Iterable

from pandas import DataFrame, Series
from technical import qtpylib

from freqtrade.strategy import CategoricalParameter
from mixins.base import MixinStrategy


class MACDBuyParams:
    buy_macd_fast = CategoricalParameter(list(range(5, 30, 3)), default=11)
    buy_macd_slow = CategoricalParameter(list(range(25, 50, 3)), default=25)
    buy_macd_signal = CategoricalParameter(list(range(5, 30, 3)), default=8)


class MACDSellParams:
    sell_macd_fast = CategoricalParameter(list(range(5, 30, 3)), default=11)
    sell_macd_slow = CategoricalParameter(list(range(25, 50, 3)), default=25)
    sell_macd_signal = CategoricalParameter(list(range(5, 30, 3)), default=8)


class MACDIndicatorMixin:

    def calculate_macd(
            self,
            series: Series,
            space: str,
            macd_fast_range: Iterable[int],
            macd_slow_range: Iterable[int],
            macd_signal_range: Iterable[int],
    ) -> List[DataFrame]:
        frames = []
        for macd_fast, macd_slow, macd_signal in itertools.product(
                macd_fast_range,
                macd_slow_range,
                macd_signal_range,
        ):
            macd = qtpylib.macd(series, fast=macd_fast, slow=macd_slow, smooth=macd_signal)
            macd_key = f'{space}_macd_f{macd_fast}|s{macd_slow}|sig{macd_signal}'
            signal_key = f'{space}_macdsignal_f{macd_fast}|s{macd_slow}|sig{macd_signal}'
            hist_key = f'{space}_macdhist_f{macd_fast}|s{macd_slow}|sig{macd_signal}'
            frames.append(DataFrame({
                macd_key: macd['macd'],
                signal_key: macd['signal'],
                hist_key: macd['histogram']
            }))
        return frames


class MACDBuyMixin(MACDBuyParams, MACDIndicatorMixin, MixinStrategy):

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        return super().populate_indicator_frames(dataframe) + self.calculate_macd(
            dataframe["close"],
            "buy",
            self.buy_macd_fast.range,
            self.buy_macd_slow.range,
            self.buy_macd_signal.range
        )

    def populate_buy_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        conditions = super().populate_buy_conditions(dataframe, metadata)
        macd_fast = self.buy_macd_fast.value
        macd_slow = self.buy_macd_slow.value
        macd_signal = self.buy_macd_signal.value
        conditions.append(
            qtpylib.crossed_above(
                dataframe[f'buy_macd_f{macd_fast}|s{macd_slow}|sig{macd_signal}'],
                dataframe[f'buy_macdsignal_f{macd_fast}|s{macd_slow}|sig{macd_signal}']
            )
        )
        return conditions


class MACDSellMixin(MACDSellParams, MACDIndicatorMixin, MixinStrategy):

    def populate_indicator_frames(self, dataframe) -> List[DataFrame]:
        return super().populate_indicator_frames(dataframe) + self.calculate_macd(
            dataframe["close"],
            "sell",
            self.sell_macd_fast.range,
            self.sell_macd_slow.range,
            self.sell_macd_signal.range
        )

    def populate_sell_conditions(self, dataframe: DataFrame, metadata: dict) -> List[Series]:
        conditions = super().populate_buy_conditions(dataframe, metadata)
        macd_fast = self.sell_macd_fast.value
        macd_slow = self.sell_macd_slow.value
        macd_signal = self.sell_macd_signal.value
        conditions.append(
            qtpylib.crossed_below(
                dataframe[f'sell_macd_f{macd_fast}|s{macd_slow}|sig{macd_signal}'],
                dataframe[f'sell_macdsignal_f{macd_fast}|s{macd_slow}|sig{macd_signal}']
            )
        )
        return conditions


class MACDMixin(MACDBuyMixin, MACDSellMixin):
    """
    Moving Average Convergence Divergence

    https://www.investopedia.com/terms/m/macd.asp
    """
