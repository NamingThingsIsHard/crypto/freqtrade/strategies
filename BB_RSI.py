"""
strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from functools import reduce

import freqtrade.vendor.qtpylib.indicators as qtpylib
import pandas
import talib.abstract as ta
from freqtrade.strategy import CategoricalParameter, IStrategy, IntParameter
from pandas import DataFrame


class BollingerBandRsiStrategy(IStrategy):
    """
    Use Bollinger Bands as first indicator and RSI to ensure positions are better than just
     BBs alone.
    """

    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Minimal ROI designed for the strategy.
    # This attribute will be overridden if the config file contains "minimal_roi".
    minimal_roi = {
        "0": 10
    }

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    timeframe = "1h"

    # Trailing stoploss
    trailing_stop = False

    # Hyperopt params
    buy_rsi = IntParameter(20, 40, default=30)
    bb_std = IntParameter(1, 4, default=2, space="buy")
    bb_window = IntParameter(14, 30, default=20, space="buy")

    sell_rsi_enable = CategoricalParameter([True, False], default=False)
    sell_rsi = IntParameter(60, 100, default=70)

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe["rsi"] = ta.RSI(dataframe)

        # Bollinger Bands
        frames = [dataframe]
        for bb_std in self.bb_std.range:
            for bb_window in self.bb_window.range:
                bollinger = qtpylib.bollinger_bands(
                    qtpylib.typical_price(dataframe),
                    window=bb_window, stds=bb_std)
                frames.append(DataFrame({
                    f'bb_lowerband_{bb_std}_{bb_window}': bollinger['lower'],
                    f'bb_middleband_{bb_std}_{bb_window}': bollinger['mid'],
                    f'bb_upperband_{bb_std}_{bb_window}': bollinger['upper'],
                }))
        return pandas.concat(frames, axis=1)

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        conditions = []
        # "Guard"
        conditions.append(dataframe['rsi'] > self.buy_rsi.value)

        # "Trigger"
        bb_std = self.bb_std.value
        bb_window = self.bb_window.value
        conditions.append(dataframe['close'] < dataframe[f'bb_lowerband_{bb_std}_{bb_window}'])

        if conditions:
            dataframe.loc[
                reduce(lambda x, y: x & y, conditions),
                'buy'] = 1
        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        conditions = []

        # "Guard"
        if self.sell_rsi_enable.value:
            conditions.append(dataframe['rsi'] < self.sell_rsi.value)

        # "Trigger"

        bb_std = self.bb_std.value
        bb_window = self.bb_window.value
        conditions.append(dataframe["close"] >= dataframe[f'bb_upperband_{bb_std}_{bb_window}'])

        if conditions:
            dataframe.loc[
                reduce(lambda x, y: x & y, conditions),
                'sell'] = 1
        return dataframe
