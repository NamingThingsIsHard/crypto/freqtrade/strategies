"""
Freqtrade/Strategies
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import pandas as pd
import talib.abstract as ta
from freqtrade.strategy import CategoricalParameter, IStrategy, IntParameter
from pandas import DataFrame
from technical import qtpylib


class VWAP_EMA(IStrategy):
    """
    Possibly good for longterm strategy
    """
    # Strategy interface version - allow new iterations of the strategy interface.
    # Check the documentation or the Sample strategy to get the latest version.
    INTERFACE_VERSION = 2

    # Optimal stoploss designed for the strategy.
    # This attribute will be overridden if the config file contains "stoploss".
    stoploss = -0.05
    timeframe = "1h"

    buy_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="EMA")
    buy_ma_period = IntParameter(5, 30, default=9)

    sell_ma_type = CategoricalParameter(["EMA", "SMA", "TEMA", "WMA"], default="EMA")
    sell_ma_period = IntParameter(5, 30, default=9)

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        frames = [dataframe]
        for func_name in self.buy_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.buy_ma_period.range:
                frame = DataFrame()
                frame[f"buy_{func_name}_{val}"] = function(dataframe, timeperiod=val)
                frames.append(frame)
        for func_name in self.sell_ma_type.range:
            function = getattr(ta, func_name)
            for val in self.sell_ma_period.range:
                frame = DataFrame()
                frame[f"sell_{func_name}_{val}"] = function(dataframe, timeperiod=val)
                frames.append(frame)

        toreturn = pd.concat(frames, axis=1)
        return toreturn

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        #  bp  p  c
        #  x      x
        #      x
        moving_average = dataframe[f"buy_{self.buy_ma_type.value}_{self.buy_ma_period.value}"]
        dataframe.loc[
            (
                qtpylib.crossed_above(moving_average, qtpylib.vwap(dataframe))
            ),
            'buy'] = 1

        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        moving_average = dataframe[f"sell_{self.sell_ma_type.value}_{self.sell_ma_period.value}"]
        dataframe.loc[
            (
                qtpylib.crossed_below(moving_average, qtpylib.vwap(dataframe))
            ),
            'sell'] = 1

        return dataframe
